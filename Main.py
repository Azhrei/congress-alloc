import csv
import math
import time

NUM_MEMBERS: int = 435


def current_milli_time():
    return int(round(time.time() * 1000))


class State:
    def __init__(self, state, population, members=1):
        self.state = state
        self.population = population
        self.members = members
        self.priority = calc_priority(self.population, self.members)

    def __repr__(self):
        return f'(state: {self.state}, population: {self.population}, members: {self.members}, priority: {self.priority})'


def compute_allocation(populations, year):
    allocated = 0

    state_list = []
    for pop_row in populations:
        state_list.append(State(pop_row["State"], int(pop_row[year].replace(',', ''))))

        allocated += 1

    while allocated < NUM_MEMBERS:
        state_list.sort(key=lambda item: item.priority, reverse=True)
        state_list[0].members += 1
        allocated += 1
        for state in state_list:
            state.priority = calc_priority(state.population, state.members)

    state_list.sort(key=lambda item: item.state)
    return state_list


def calc_priority(pop, members):
    if members > 0:
        priority = pop / math.sqrt(members * (members + 1))
    else:
        priority = 0
    return priority


def compare(calculated):
    total_difference = 0

    with open('data/actual_results.csv', mode='r') as csv_file:
        csv_reader = csv.reader(csv_file)
        for row in csv_reader:
            calc_state = next((x for x in calculated if x.state == row[0]), State(row[0], 0, members=0))
            current_members = int(row[1])

            if calc_state.members > current_members:
                difference = calc_state.members - current_members
                print(f'{row[0]} has {calc_state.members} members, {difference} more than current ({current_members})')
                total_difference += difference
            elif calc_state.members < current_members:
                difference = current_members - calc_state.members
                print(f'{row[0]} has {calc_state.members} members, {difference} less than current ({current_members})')
                total_difference += difference

    print(f'Total change of {total_difference} members')
    return


def start(file_path, year, print_full):
    start_time = current_milli_time()

    with open(file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        results = compute_allocation(csv_reader, year)
        compare(results)
        if print_full:
            print(f'{results}')

    finish_time = current_milli_time()
    print(f'Finished in {finish_time - start_time}ms')


# Change these values for different results
# e.g. To see the effect adding Puerto Rico and DC as states would have, change to 'data/full_pop_data.csv'
# e.g. To see the full results, change 'False' to 'True'
start('data/states_only.csv', 'Census', False)
